# Civo Navigate 2023 Talk: Orchestrating Kubernetes with Terraform & Kustomize

Repository for my talk given at Civo Navigate 2023

## Presentation
* [Google Drive](https://docs.google.com/presentation/d/14FqyIA1oNl7RY1zq42ZN7W6gcV_sLROlAhkq94lSNwc/edit?usp=sharing)

## Running
* [Terraform](terraform/README.md)
* [Kubernetes](kubernetes/README.md)

## Useful Links
* [Civo's Docs on Getting Up and Going with Terraform](https://www.civo.com/docs/overview/terraform)
* [Civo's Terraform Provider](https://registry.terraform.io/providers/civo/civo/latest)
