locals {
  # This lets us take the name given for an environment node size
  # and convert it to a number (0-4) that we use below in
  # data.civo_size.civo_sizes for determining the correct Civo size
  # to map to
  civo_size_conversion = [
    "xsmall",
    "small",
    "medium",
    "large",
    "xlarge",
  ]
}

resource "civo_kubernetes_cluster" "clusters" {
  for_each    = var.environments
  name        = each.value.name
  firewall_id = civo_firewall.environment_firewall[each.value.name].id
  pools {
    label = "default"
    # Getting the name of the Civo node size that we want to use based
    # on the provided node size for the environment
    size       = element(data.civo_size.civo_sizes.sizes, index(local.civo_size_conversion, each.value.node_size)).name
    node_count = each.value.num_nodes
  }
}

# Pulling all of the available Civo node sizes sorted by memory
data "civo_size" "civo_sizes" {
  filter {
    key    = "type"
    values = ["kubernetes"]
  }

  sort {
    key       = "ram"
    direction = "asc"
  }
}
