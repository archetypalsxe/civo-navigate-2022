resource "civo_firewall" "environment_firewall" {
  for_each = var.environments
  name     = each.value.name
}
