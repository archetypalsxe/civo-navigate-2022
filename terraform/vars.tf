variable "civo_token" {
  description = "The Civo token for your account"
  type        = string
}

variable "environments" {
  description = "Settings for all the environments to deploy"
  type = map(object({
    name      = string
    num_nodes = number
    # Allowed values:
    # [xsmall, small, medium, large, xlarge]
    node_size = string
    region    = string
  }))
  default = {
    "dev" = {
      name      = "dev"
      num_nodes = 1
      node_size = "xsmall"
      region    = "NYC1"
    }
    "prod" = {
      name      = "prod"
      num_nodes = 2
      node_size = "small"
      region    = "NYC1"
    }
  }
}
