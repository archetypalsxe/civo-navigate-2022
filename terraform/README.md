# Terraform

## Running

### Credentials
* If you run terraform commands without any changes, it will prompt you for your Civo token
* My recommendation is to create a `terraform.tfvars` file inside of this (`terraform`) directory
  * This will prevent you from having to enter your token every time you run a Terraform command
  * The file only needs one line like this:
    * `civo_token = "XXXX1234XXXX4321"`
      * Except replace the text between the quotes with your own token
        * Accessible through the Civo dashboard

### Applying Terraform
From the `terraform` directory:
* `terraform init`
* `terraform plan`
* `terraform apply`

### Cleaning Up / Removing
From the `terraform` directory:
* `terraform destroy`
