# Kubernetes

## Configuration
* Looks like you may have to download the kubeconfig from the Civo UI
  * Log into [the dashboard](dashboard.civo.com)
  * Access `Kubernetes` on the side panel
  * Click on the appropriate cluster
  * You should see on the page: `Kubeconfig: Click to download`
  * Kubeconfig can be downloaded here
  * Should either be saved as `~/.kube/config` or remove the cluster, contexts and users and integrate with existing config


## Applying / Running
* From the appropriate overlay/{$environment:?} directory
* Run `kubectl apply -k .`
  * Runs kustomize to generate the kubernetes manifest
  * Applies the generated manifests
* `kubectl get svc` can be used to obtain the public IP
  * May take a few for it to populate once creating
  * Generally seems to take under a minute

### Troubleshooting
* The service is not obtaining a public IP
  * Check the status of the pods
    * If they aren't fully running, it could block the service from starting
* Pods aren't able to pull the nginx docker image from docker hub
  * It seems like if you recently created the network it takes a while for internet connectivity to be active
    * Seems like if you wait some time... 5-10 mins (?) it'll automatically start working

## Deleting Everything / Cleaning Up
* From the appropriate overlay/{$environment:?} directory
* Run `kubectl delete -k .`
  * Runs kustomize to generate the kubernetes manifest
  * Removes these manifests accordingly
